"use strict";

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(value) {
        this._name = value;
    }

    get name() {
        return this._name;
    }

    set age(value) {
        this._age = value;
    }

    get age() {
        return this._age;
    }

    set salary(value) {
        this._salary = value;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }
}

let programmer1 = new Programmer("Peter", 33, 3500, ['english', 'spanish']);
let programmer2 = new Programmer("Sofi", 25, 2500, ['english', 'french']);
let programmer3 = new Programmer("Oliver", 27, 3200, ['english', 'germany']);
let programmer4 = new Programmer("Anna", 30, 3200, ['english', 'italian']);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
console.log(programmer4);